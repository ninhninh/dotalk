
var audios = new Array();
$(document).ready(function(){    
    initAudio();    
    $('.decored-bar').on('click',function(){
        var playbtn = $(this).children('a');
        var playicon = $(this).find('.icon');
        var aid = $(playbtn).attr('aid');        
        if($(playbtn).attr('stt')=='isplay'){
            audios[aid].pause();  
            $(playbtn).attr('stt','ispause');
            $(playicon).removeClass('pause-icon');
            $(playicon).addClass('play-icon');
        }else{
            audios[aid].play();  
            $(playbtn).attr('stt','isplay');
            $(playicon).removeClass('play-icon');
            $(playicon).addClass('pause-icon');            
            showDuration(aid);
        }
    })
})

function initAudio(){
    var audio_element = $('audio');
    $.each(audio_element, function(key,element){
        if(!$(element).hasClass('tranformed'))
        {   
            var aid = randomstr(5)+key;
            $(element).addClass('tranformed hide');                            
            var src = $(element).find('source').first().attr('src');
            audio = new Audio(src);            
            audios[aid]=audio;  
            getDuaration(aid);
            var audio_html = "<div class='decored-bar'><a role='button' aid='"+aid+"' href='#'><span><i class='icon play-icon'>&nbsp;</i></span><span class='time time-"+aid+"' >0:00</span><div class='line'>&nbsp;</div></a><div class='time-run duaration-"+aid+"'><a role='button' aria-disabled='true' href='#'><span><i class='icon play-icon'>&nbsp;</i></span><span class='time time-"+aid+"' ></span><div class='line'>&nbsp;</div></a></div></div>";                            
            $(element).after(audio_html);
        }                
    });
}    
function randomstr(len){
    charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}

function getDuaration(aid){
    $(audios[aid]).bind('loadedmetadata',function(){        
        var s = parseInt(audios[aid].duration % 60);
        var m = parseInt(audios[aid].duration / 60) % 60;
        if(s < 10){
            s = '0'+s;
        }
        $('.time-'+aid).text(m+":"+s);
    });
}

function showDuration(aid){
    $(audios[aid]).bind('timeupdate',function(){        
        var value = 0;
        if(audios[aid].currentTime > 0){
            value = Math.floor((100 / audios[aid].duration) * audios[aid].currentTime);
        }
        $('.duaration-'+aid).css('width',value+'%');
    });
}