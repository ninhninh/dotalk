
$(document).ready(function(){
    $(".navbar-toggle").click(function(e) {        
        if($(this).data('target')=="navbar-left")
        {
          $("#wrapper").removeClass("toggled-right");
          $("#wrapper").toggleClass("toggled");
        }else{
          $("#wrapper").removeClass("toggled");
          $("#wrapper").toggleClass("toggled-right");
        }     
    });
	$(".top-mess-icon").click(function(e) { 
          $(".video-call-content").toggleClass("toggled");
		  $(".chat-while-video-call").toggleClass("show");
		  $(".audio-call-content").toggleClass("toggled");
    });
    $(".recent-list .friend ").click(function (e) {
		$(this).addClass("active").siblings().removeClass("active");
	});
	$('.mem-list .friend').on('click',function(){
		$(this).toggleClass('active');
	})
	$(".search-wrapper input").focus(function (e) {
	  $(this).parent().find(".actions").addClass("active");
	});
	$('.add-friend-icon').click(function(){
		$('.autocomplate').fadeToggle( 300, "linear" );

	})
	$(".search-wrapper input").focusout(function (e) {
	  if($(this).val() == ""){
		$(this).parent().find(".actions").removeClass("active");
	}   
    });
	
	$(".favorite-icon").click(function(){
        $(this).toggleClass("start-fill");
    })
	$('.chat-input-tools-btn').click(function(){
		var target = $(this).data('target');
		$(".tool-popup").each(function(key,obj){
			if($(obj).attr('id')!=target){
				$(obj).addClass('hidden');
			}
		});
		$("#"+target).toggleClass("hidden");
	});
	
	$(document).mouseup(function(e) 
	{
		var container = $(".toolbox");

		if (!container.is(e.target) && container.has(e.target).length === 0) 
		{
			$(".tool-popup").addClass('hidden'	);
		}
	});
	
	$(".add-more-friend").click(function(){
        $(".add-users-box ").toggleClass("hidden");
    })
	
    $(".record-btn").click(function(){
        $(".voice-chat-box").toggleClass("before-rec");
        $(".voice-chat-box").toggleClass("after-rec");
    })
	$(".num-member").click(function(){
        $(this).toggleClass("expanding");
    })
	$(".tool-item").click(function(){
        $(this).toggleClass("active");
    })
	$(".show-screen-icon").click(function(){
        $(".show-webcam").toggleClass("hidden");
		$(this).toggleClass("show");
    })
	$("#change_name").focus(function(){
        $(".change-name-icon").toggleClass("hidden");
    })
	$(".left-side-bar-footer button").click(function(){
        $(this).toggleClass("active");
    })
	$(".plans-control .guide").click(function(){
		$(".btn-control").css("left", 51 + "%");
		 $(this).addClass("active").siblings().removeClass("active");
	});
	$(".plans-control .pricing").click(function(){
		$(".btn-control").css("left", 5 + "px");
		$(this).addClass("active").siblings().removeClass("active");
	});
	$(".take-photo-btn").click(function(){
		$(".overlay").removeClass("hidden");
		$(".sidebar").css("z-index", 10);
		$(".chatbox-header").css("z-index", 0);
	})
	$(".cammera-box .close-form").click(function(){
		$("#take_photo_popup").addClass("hidden");
		$(".overlay").addClass("hidden");
	})
	$(".pricing-item").click(function(){
		$(this).addClass("active").siblings().removeClass("active");
	})
	
    $(function(){
      $('.recent-list').slimScroll({
      height: 'calc(100vh - 135px)',
      });
	  $('.conact-list').slimScroll({
      height: 'calc(100vh - 135px)',
      }); 
	  $('.channel-list').slimScroll({
      height: 'calc(100vh - 135px)',
      }); 	  
      $('.chatbox .chatbox-content').slimScroll({
      height: 'calc(100vh - 160px)',
      });
	  $('.chatbox-content.video').slimScroll({
      height: '205px',
      });	  
	  $('.ads-wrap').slimScroll({
      height: 'calc(100vh - 145px)',
      });
	  $('.add-friend-wrap').slimScroll({
      height: 'calc(100vh - 145px)',
      });	  
	  $('.pending-wrap').slimScroll({
      height: 'calc(100vh - 70px)',
      });
	  $('.add-mem-wrap').slimScroll({
		height:'290px'
      });
	  $('.autocomplate .list').slimScroll({
		height:'460px'
      });
	  $('.create-channel-form .add-mem-wrap').slimScroll({
		height:'400px'
      });
	   $('.create-form .add-member .autocomplate .list').slimScroll({
		height:'280px'
      });
	  $('.sidebar-nav-content').slimScroll({
		height: 'calc(100vh - 250px)',
      });
	  	  
	  $('.group-header .add-member').slimScroll({
		  height: '98px',			
	  });
	  $('.member-wrap').slimScroll({
		  height: '60vh',			
	  });
	  $('.member-wrap').slimScroll({
		  height: '60vh',			
	  });
	   $('.add-contact-wrap').slimScroll({
		  height: '100vh - 70px',			
	  });
	   $('.video-call-form.group .group-cam').slimScroll({
			height: 'calc(100vh - 170px)',
	   })
	  $(".pop-up-payment .close").click(function(){
		  $(".pop-up-payment").addClass("hidden");
		  $(".overlay").addClass("hidden");
	  })
		$(".btn-process .btn").click(function(){
			$(".pop-up-payment").removeClass("hidden");
		  $(".overlay").removeClass("hidden");
		})
  });
})
    